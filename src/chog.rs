use macroquad::prelude::*;

pub const JUMP_SPEED: f32 = -650.0;
pub const GRAVITY: f32 = 2000.0;
pub const MOVE_SPEED: f32 = 5.0;

#[derive(PartialEq)]
pub struct ChogTextures {
    // I might want to add more
    pub still: Texture2D,
    pub left: Texture2D,
    pub right: Texture2D,
    // I still need to add an up texture for most things
    pub up: Texture2D,
}

pub fn new_chog() -> ChogTextures {
    let chog_right =
        Texture2D::from_file_with_format(include_bytes!("../assets/chog/chog_right_64.png"), None);
    let chog_left =
        Texture2D::from_file_with_format(include_bytes!("../assets/chog/chog_left_64.png"), None);
    // Add the textures for other positions
    ChogTextures {
        still: chog_right,
        left: chog_left,
        right: chog_right,
        up: chog_right,
    }
}

pub fn new_dog() -> ChogTextures {
    let dog_right =
        Texture2D::from_file_with_format(include_bytes!("../assets/chog/dog_right_64.png"), None);
    let dog_left =
        Texture2D::from_file_with_format(include_bytes!("../assets/chog/dog_left_64.png"), None);
    // Add the other textures
    ChogTextures {
        still: dog_right,
        left: dog_left,
        right: dog_right,
        up: dog_right,
    }
}

pub fn new_chicken() -> ChogTextures {
    let chicken_right = Texture2D::from_file_with_format(
        include_bytes!("../assets/chog/chicken_split_right_64.png"),
        None,
    );
    // Add the other textures
    let chicken_left = Texture2D::from_file_with_format(
        include_bytes!("../assets/chog/chicken_split_left_64.png"),
        None,
    );
    ChogTextures {
        still: chicken_right,
        left: chicken_left,
        right: chicken_right,
        up: chicken_right,
    }
}
