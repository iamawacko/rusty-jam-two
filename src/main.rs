use crate::miniquad::conf::Icon;
use macroquad::prelude::*;
use macroquad::ui::{root_ui, Skin};

pub mod chog;
pub mod common;
mod first;
mod second;
mod secretlevel;
mod tutorial;

fn window_conf() -> macroquad::window::Conf {
    macroquad::window::Conf {
        window_title: "Chog Life".to_owned(),
        high_dpi: true,
        // Other aspect ratio's are hard
        window_resizable: false,
        icon: Some(Icon {
            small: common::SMALL_ICON,
            medium: common::MEDIUM_ICON,
            big: common::BIG_ICON,
        }),
        ..Default::default()
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    // I haven't used the UI anywhere yet
    let _skin1 = {
        let label_style = root_ui()
            .style_builder()
            .font(include_bytes!("../assets/ui/HTOWERT.TTF"))
            .unwrap()
            .text_color(Color::from_rgba(180, 180, 120, 255))
            .font_size(30)
            .build();
        let window_style = root_ui()
            .style_builder()
            .background(Image::from_file_with_format(
                include_bytes!("../assets/ui/window_background.png"),
                None,
            ))
            .background_margin(RectOffset::new(20.0, 20.0, 10.0, 10.0))
            .margin(RectOffset::new(-20.0, -30.0, 0.0, 0.0))
            .build();
        let button_style = root_ui()
            .style_builder()
            .background(Image::from_file_with_format(
                include_bytes!("../assets/ui/button.png"),
                None,
            ))
            .background_margin(RectOffset::new(37.0, 37.0, 5.0, 5.0))
            .margin(RectOffset::new(10.0, 10.0, 0.0, 0.0))
            .background_hovered(Image::from_file_with_format(
                include_bytes!("../assets/ui/button_hovered.png"),
                None,
            ))
            .background_clicked(Image::from_file_with_format(
                include_bytes!("../assets/ui/button_clicked.png"),
                None,
            ))
            .font(include_bytes!("../assets/ui/HTOWERT.TTF"))
            .unwrap()
            .text_color(Color::from_rgba(180, 180, 100, 255))
            .font_size(40)
            .build();
        let editbox_style = root_ui()
            .style_builder()
            .background_margin(RectOffset::new(0., 0., 0., 0.))
            .font(include_bytes!("../assets/ui/HTOWERT.TTF"))
            .unwrap()
            .text_color(Color::from_rgba(120, 120, 120, 255))
            .color_selected(Color::from_rgba(190, 190, 190, 255))
            .font_size(50)
            .build();

        Skin {
            editbox_style,
            window_style,
            button_style,
            label_style,
            ..root_ui().default_skin()
        }
    };

    // CC0 music. Has to be cut down, as 10 MB is a lot
    let music = macroquad::audio::load_sound_from_bytes(include_bytes!(
        "../assets/sound/624886__sonically-sound__video-music-128-bpm.ogg"
    ))
    .await
    .unwrap();

    macroquad::audio::play_sound(
        music,
        macroquad::audio::PlaySoundParams {
            looped: true,
            volume: 0.5,
        },
    );

    info!("Game starting");
    let mut level_counter = 0;
    loop {
        clear_background(BLACK);

        // I could probably use a match here
        if level_counter == 0 {
            tutorial::start().await;
            set_default_camera();
            level_counter += 1;
        } else if level_counter == 1 {
            if first::start().await {
                set_default_camera();
                level_counter += 1;
            } else {
                set_default_camera();
            }
        } else if level_counter == 3 {
            second::start().await;
            set_default_camera();
            level_counter += 1;
        } else if level_counter == 2 {
            draw_text("Thank You", 1., screen_height() / 2., 70.0, YELLOW);
            draw_text("For Playing", 1., (screen_height() / 2.) + 70., 70., YELLOW);
        }
        // Maybe someone gets the reference
        if is_key_down(KeyCode::M) {
            level_counter = 267;
        }
        if level_counter == 267 {
            info!("Entered secret level");
            secretlevel::start().await;
        }

        next_frame().await
    }
}
