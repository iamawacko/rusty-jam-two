use crate::chog::*;
use macroquad::prelude::*;
use macroquad_platformer::*;
use macroquad_tiled as tiled;

struct Player {
    collider: Actor,
    speed: Vec2,
}

pub async fn start() {
    info!("Starting second level");

    // Add the textures for other positions
    let chogs = new_chog();
    let dogs = new_dog();
    let chickens = new_chicken();

    let scifi_tileset = Texture2D::from_file_with_format(
        include_bytes!("../assets/scifi_sidescroller_tiles.png"),
        None,
    );
    let scifi_tileset2 = Texture2D::from_file_with_format(
        include_bytes!("../assets/scifi_platformTiles_32x32.png"),
        None,
    );
    let tutorial_map_json = include_str!("../assets/tutorial.json").to_string();
    let tiled_map = tiled::load_map(
        &tutorial_map_json,
        &[
            ("scifi_sidescroller_tiles.png", scifi_tileset),
            ("scifi_platformTiles_32x32.png", scifi_tileset2),
        ],
        &[],
    )
    .unwrap();
    debug!("Textures loaded");

    debug!("Loading sounds");
    // This sound is also CC0
    let chicken_sound = macroquad::audio::load_sound_from_bytes(include_bytes!(
        "../assets/sound/192035__dann93__chickens_simple.wav"
    ))
    .await
    .unwrap();
    debug!("Sounds loaded");

    let width = tiled_map.raw_tiled_map.tilewidth * tiled_map.raw_tiled_map.width * 2;
    let height = tiled_map.raw_tiled_map.tileheight * tiled_map.raw_tiled_map.height * 2;

    let mut static_colliders = vec![];
    // This misses some tiles
    for (_x, _y, tile) in tiled_map.tiles("Main Layer", None) {
        if tile.is_some() {
            static_colliders.push(Tile::Collider);
        } else {
            static_colliders.push(Tile::Empty);
        }
    }

    let mut world = World::new();
    world.add_static_tiled_layer(
        static_colliders,
        (tiled_map.raw_tiled_map.tilewidth * 2) as f32,
        (tiled_map.raw_tiled_map.tileheight * 2) as f32,
        (tiled_map.raw_tiled_map.width) as _,
        1,
    );

    let mut player = Player {
        collider: world.add_actor(vec2(200.0, 100.0), 64, 64),
        speed: vec2(0., 0.),
    };

    // Seeding random
    macroquad::rand::srand(get_time() as u64);

    let mut sprites = &chogs;
    let mut sprite = sprites.still;
    let mut chicken = false;
    let mut chog_pos = world.actor_pos(player.collider);
    let mut chicken_pos = chog_pos;
    loop {
        chog_pos = world.actor_pos(player.collider);
        clear_background(BLACK);
        tiled_map.draw_tiles(
            "Main Layer",
            Rect::new(0.0, 0.0, width as _, height as _),
            None,
        );
        tiled_map.draw_tiles(
            "Background",
            Rect::new(0.0, 0.0, width as _, height as _),
            None,
        );
        tiled_map.draw_tiles("Exit", Rect::new(0.0, 0.0, width as _, height as _), None);

        //let chicken_movement = macroquad::rand::rand();
        //if chicken_movement > ((u32::MAX / 3) * 2) && chicken {
        //    chicken_pos.x = chicken_pos.x + 10.;
        //} else if chicken_movement < (u32::MAX / 3) && chicken {
        //    chicken_pos.x = chicken_pos.x - 10.;
        //} else if chicken {
        //    chicken_pos.y = chicken_pos.y - 10.;
        //}

        draw_texture_ex(
            sprite,
            chog_pos.x,
            chog_pos.y,
            WHITE,
            DrawTextureParams {
                source: Some(Rect::new(0.0, 0.0, 64., 64.)),
                ..Default::default()
            },
        );

        if chicken {
            draw_texture_ex(
                chickens.still,
                chicken_pos.x,
                chicken_pos.y,
                WHITE,
                DrawTextureParams {
                    source: Some(Rect::new(0.0, 0.0, 64., 64.)),
                    ..Default::default()
                },
            );
        }

        let on_ground = world.collide_check(player.collider, chog_pos + vec2(0., 1.));
        if !on_ground {
            player.speed.y += GRAVITY * get_frame_time();
        }

        if is_key_down(KeyCode::Q) {
            break;
        }
        if is_key_down(KeyCode::Escape) {
            break;
        }
        // I have to add support for other key patterns, like WASD and HJKL
        if is_key_down(KeyCode::Right) {
            player.speed.x += MOVE_SPEED;
            sprite = sprites.right;
        } else if is_key_down(KeyCode::Left) {
            player.speed.x -= MOVE_SPEED;
            sprite = sprites.left;
        } else {
            player.speed.x = 0.;
        }

        let chicken_collision = (chicken_pos.x < chog_pos.x + 64.
            && chicken_pos.x + 64. > chog_pos.x
            && chicken_pos.y < chog_pos.y + 64.
            && chicken_pos.y + 64. > chog_pos.y)
            && chicken;

        if is_key_pressed(KeyCode::Up) && (on_ground || chicken_collision) {
            player.speed.y = JUMP_SPEED;
        }

        if player.speed.x > 500. {
            player.speed.x = 500.;
        } else if player.speed.x < -500. {
            player.speed.x = -500.;
        }

        // The code for splitting up
        if is_key_pressed(KeyCode::Space) {
            if *sprites == chogs {
                sprites = &dogs;
                sprite = sprites.still;
                chicken = true;
                chicken_pos = chog_pos;
                chicken_pos.x += 30.;
                macroquad::audio::play_sound(
                    chicken_sound,
                    macroquad::audio::PlaySoundParams {
                        looped: false,
                        volume: 0.9,
                    },
                );
            } else {
                sprites = &chogs;
                chicken = false;
            }
        }

        if chicken_collision {
            if chicken_pos.x < chog_pos.x + 64. {
                world.move_h(player.collider, 200. * get_frame_time());
            } else if chicken_pos.x + 64. > chog_pos.x {
                world.move_h(player.collider, -200. * get_frame_time());
            }
            if chicken_pos.y < chog_pos.y + 56. {
                world.move_v(player.collider, 2. * get_frame_time());
            } else if chicken_pos.y + 56. > chog_pos.y {
                world.move_v(player.collider, -2010. * get_frame_time());
            }
        } else {
            world.move_h(player.collider, player.speed.x * get_frame_time());
            world.move_v(player.collider, player.speed.y * get_frame_time());
        }

        // The camera has been a bit of a pain, but it mostly works now
        let mut camera_pos = chog_pos.x;
        if camera_pos > 2700. {
            camera_pos = 2700.;
        }
        if camera_pos < 490. {
            camera_pos = 490.;
        }
        set_camera(&Camera2D {
            zoom: vec2(0.002, -0.003),
            target: vec2(camera_pos, 345.),
            ..Default::default()
        });

        if tiled_map
            .get_tile("Exit", chog_pos.x as u32, chog_pos.y as u32)
            .is_some()
        {
            break;
        }

        next_frame().await
    }
    info!("Ended second level");
}
