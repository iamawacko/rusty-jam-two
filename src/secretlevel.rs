use macroquad::prelude::*;

pub async fn start() {
    // Rust superiorty :)
    let image =
        Texture2D::from_file_with_format(include_bytes!("../assets/secretlevel/image.png"), None);
    loop {
        clear_background(BLACK);
        draw_texture_ex(
            image,
            0.,
            0.,
            WHITE,
            DrawTextureParams {
                ..Default::default()
            },
        );
        next_frame().await
    }
}
