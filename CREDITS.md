## Textures

Sci-Fi Platform Tiles
by Eris <br />
CC0
https://opengameart.org/content/sci-fi-platform-tiles


Sci-Fi Sidescroller Tiles
by surt <br />
CC0
https://opengameart.org/content/sci-fi-scidescroller-tiles

## Sound

Loopable Track for Videos and Podcasts (128 BPM)
by sonically_sound <br />
CC0
https://freesound.org/people/sonically_sound/sounds/624886/


Chickens
by Dann93 <br />
CC0
https://freesound.org/people/Dann93/sounds/192035/
