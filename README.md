# Chog Life
You are a failed experiment, a monster born of dog and chicken. Some might call you a Chog. You posses a searing desire to take out the person who created you. You can split yourself back into a dog and a chicken, but a chicken that has trouble thinking and a dog with only half a body can't do much. Or can they? 

## Controls
Arrow keys for movement, space key to split apart.

## MSRV
rustc 1.58.1

## Todo
- [x] Scrolling
- [ ] Mobile support (I daily drive the Pinephone, so I don't just mean Android and iOS)
- [x] WASM Support
- [x] CI/CD Pipeline
- [ ] More levels
- [ ] A win condition
- [ ] Chicken Movement
- [ ] CI/CD Integration with Itch.io
